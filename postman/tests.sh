#!/bin/bash

#/users/all - получить инфо о всех пользователях
curl --location 'http://localhost:8008/users/all'

#/users - получить инфо о всех активных пользователях
curl --location 'http://localhost:8008/users'

#/users - добавить пользователя login_1
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "kiriakos",
    "firstName": "Кириакос",
    "lastName": "Пападопулос",
    "email": "kiriakos@email.com",
    "birthDate": "2000-01-01"
}'

#/users - добавить пользователя login_2
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "bom",
    "firstName": "Бом",
    "lastName": "Ким",
    "email": "bom@email.com",
    "birthDate": "2000-01-02"
}'

#/users - добавить пользователя login_3
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "dzhennifer",
    "firstName": "Дженнифер",
    "lastName": "Лопез",
    "email": "dzhennifer@email.com",
    "birthDate": "2000-01-03"
}'

#/users - добавить пользователя login_4
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "muhammed",
    "firstName": "Мухаммед",
    "lastName": "Ахмед",
    "email": "muhammed@email.com",
    "birthDate": "2000-01-04"
}'

#/users - добавить пользователя login_5
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "andrej",
    "firstName": "Андрей",
    "lastName": "Смирнов",
    "patronymic": "Петрович",
    "email": "andrej@email.com",
    "birthDate": "2000-01-05"
}'

#/users - добавить пользователя login_6
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "emma",
    "firstName": "Эмма",
    "lastName": "Траоре",
    "email": "emma@email.com",
    "birthDate": "2000-01-06"
}'

#/users - добавить пользователя login_7
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "erdogan",
    "firstName": "Эрдоган",
    "lastName": "Йылмаз",
    "email": "erdogan@email.com",
    "birthDate": "2000-01-07"
}'

#/users - добавить пользователя login_8
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "dzhek",
    "firstName": "Джек",
    "lastName": "Андерсон",
    "email": "dzhek@email.com",
    "birthDate": "2000-01-08"
}'

#/users - добавить пользователя login_9
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "benedikt",
    "firstName": "Бенедикт",
    "lastName": "Вонг",
    "email": "benedikt@email.com",
    "birthDate": "2000-01-09"
}'

#/users - добавить пользователя login_10
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "robert",
    "firstName": "Роберт",
    "lastName": "Родригес",
    "email": "robert@email.com",
    "birthDate": "2000-01-10"
}'

#/users - добавить пользователя login_11
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "dzhison",
    "firstName": "Джисон",
    "lastName": "Хан",
    "email": "dzhison@email.com",
    "birthDate": "2000-01-11"
}'

#/users - добавить пользователя login_12
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "hong",
    "firstName": "Хонг",
    "lastName": "Нгуен",
    "email": "hong@email.com",
    "birthDate": "2000-01-12"
}'

#/users - добавить пользователя login_13
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "itan",
    "firstName": "Итан",
    "lastName": "Коэн",
    "email": "itan@email.com",
    "birthDate": "2000-01-13"
}'

#/users - добавить пользователя login_14
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "brokman",
    "firstName": "Брокман",
    "lastName": "Мюллер",
    "email": "brokman@email.com",
    "birthDate": "2000-01-14"
}'

#/users - добавить пользователя login_15
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "ejsa",
    "firstName": "Эйса",
    "lastName": "Гонсалес",
    "email": "ejsa@email.com",
    "birthDate": "2000-01-15"
}'

#/users - добавить пользователя login_16
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "sanu",
    "firstName": "Сану",
    "lastName": "Кумар",
    "email": "sanu@email.com",
    "birthDate": "2000-01-16"
}'

#/users - добавить пользователя login_17
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "ali",
    "firstName": "Али",
    "lastName": "Мухаммед",
    "email": "ali@email.com",
    "birthDate": "2000-01-17"
}'

#/users - добавить пользователя login_18
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "bernardu",
    "firstName": "Бернарду",
    "lastName": "Сильва",
    "email": "bernardu@email.com",
    "birthDate": "2000-01-18"
}'

#/users - добавить пользователя login_19
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "ivan",
    "firstName": "Иван",
    "lastName": "Кузнецов",
    "patronymic": "Андреевич",
    "email": "ivan@email.com",
    "birthDate": "2000-01-19"
}'

#/users - добавить пользователя login_20
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "alberto",
    "firstName": "Альберто",
    "lastName": "Фернандес",
    "email": "alberto@email.com",
    "birthDate": "2000-01-20"
}'

#/users - добавить пользователя login_21
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "dzhokovich",
    "firstName": "Джокович",
    "lastName": "Новак",
    "email": "dzhokovich@email.com",
    "birthDate": "2000-01-21"
}'

#/users - добавить пользователя login_22
curl --location 'http://localhost:8008/users' \
--header 'Content-Type: application/json' \
--data '{
    "login": "paolo",
    "firstName": "Паоло",
    "lastName": "Росси",
    "email": "paolo@email.com",
    "birthDate": "2000-01-22"
}'

#/users/{id} - получить инфо о пользователе
curl --location 'http://localhost:8008/users/1'

#/users/{id} - обновить инфо о пользователе
curl --location --request PUT 'http://localhost:8008/users/21' \
--header 'Content-Type: application/json' \
--data '{
    "id": 21,
    "login": "dzhokovich",
    "firstName": "Джокович",
    "lastName": "Пападопулос",
    "email": "dzhokovich@email.com",
    "birthDate": "2001-01-22"
}'

#/users/{id} - обновить инфо о пользователе
curl --location --request PUT 'http://localhost:8008/users/22' \
--header 'Content-Type: application/json' \
--data '{
    "id": 22,
    "login": "paolo",
    "firstName": "Кириакос",
    "lastName": "Пападопулос",
    "email": "paolo@email.com",
    "birthDate": "2001-01-22"
}'

#/users/subscribe/{followerId}/{bloggerId} - подписаться
curl --location --request PUT 'http://localhost:8008/users/subscribe/1/2'
curl --location --request PUT 'http://localhost:8008/users/subscribe/2/1'
curl --location --request PUT 'http://localhost:8008/users/subscribe/3/1'
curl --location --request PUT 'http://localhost:8008/users/subscribe/4/1'
curl --location --request PUT 'http://localhost:8008/users/subscribe/5/1'
curl --location --request PUT 'http://localhost:8008/users/subscribe/2/3'
curl --location --request PUT 'http://localhost:8008/users/subscribe/3/4'
curl --location --request PUT 'http://localhost:8008/users/subscribe/4/5'
curl --location --request PUT 'http://localhost:8008/users/subscribe/3/5'
curl --location --request PUT 'http://localhost:8008/users/subscribe/4/3'
curl --location --request PUT 'http://localhost:8008/users/subscribe/2/3'
curl --location --request PUT 'http://localhost:8008/users/subscribe/2/4'
curl --location --request PUT 'http://localhost:8008/users/subscribe/2/5'

#/users/unsubscribe/{followerId}/{bloggerId} - отписаться
curl --location --request PUT 'http://localhost:8008/users/unsubscribe/1/2'

#/users/{id} - удалить пользователя
curl --location --request DELETE 'http://localhost:8008/users/4'

#/users/delete - получить информацию о всех удаленных пользователях
curl --location 'http://localhost:8008/users/deleted'

#/users/deleted/{id} - получить информацию об удаленном пользователе
curl --location 'http://localhost:8008/users/deleted/4'

#/users/{lastName}/{firstName} - получить все учетки пользователя
curl --location 'http://localhost:8008/users/Пападопулос/Кириакос'