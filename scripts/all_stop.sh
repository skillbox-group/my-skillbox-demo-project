#!/bin/bash

#Остановить users_app
docker stop $(docker ps -q --filter ancestor=users_app)

#Перейти в директорию с docker-compose.yaml
cd  ~/Documents/SkillboxProject/my-skillbox-demo-project/scripts/

#Остановить docker compose
docker compose stop