\connect social_network_db;

CREATE SCHEMA IF NOT EXISTS social_network_schema AUTHORIZATION owner;

CREATE SEQUENCE IF NOT EXISTS social_network_schema.user_id_seq
    START 1
    INCREMENT 20
    CACHE 1;

CREATE TABLE IF NOT EXISTS social_network_schema.users(
    id integer PRIMARY KEY DEFAULT nextval('social_network_schema.user_id_seq'),
    active boolean NOT NULL,
    login character varying NOT NULL,
    firstname character varying NOT NULL,
    lastname character varying NOT NULL,
    patronymic character varying,
    birthdate date NOT NULL,
    email character varying NOT NULL
);

CREATE TABLE IF NOT EXISTS social_network_schema.users_followers(
    follower integer,
    users_id integer REFERENCES social_network_schema.users(id)
);

CREATE TABLE IF NOT EXISTS social_network_schema.users_bloggers(
    blogger integer,
    users_id integer REFERENCES social_network_schema.users(id)
);

CREATE SEQUENCE IF NOT EXISTS social_network_schema.post_id_seq
    START 1
    INCREMENT 20
    CACHE 1;

CREATE TABLE IF NOT EXISTS social_network_schema.posts(
    id integer PRIMARY KEY DEFAULT nextval('social_network_schema.post_id_seq'),
    post character varying NOT NULL,
    users_id integer REFERENCES social_network_schema.users(id)
);

CREATE SEQUENCE IF NOT EXISTS social_network_schema.comment_id_seq
    START 1
    INCREMENT 20
    CACHE 1;

CREATE TABLE IF NOT EXISTS social_network_schema.comments(
    id integer PRIMARY KEY DEFAULT nextval('social_network_schema.comment_id_seq'),
    comment character varying NOT NULL,
    users_id integer REFERENCES social_network_schema.users(id),
    posts_id integer REFERENCES social_network_schema.posts(id)
);
