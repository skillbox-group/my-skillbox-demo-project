#!/bin/bash

mkdir -p ~/skillprojects/social_network/db/postgresql/
mkdir ~/skillprojects/social_network/db/postgresql/persistent/
mkdir ~/skillprojects/social_network/db/postgresql/docker-entrypoint-initdb.d/

cp ~/Documents/SkillboxProject/my-skillbox-demo-project/scripts/db/docker-entrypoint-initdb.d/tables.sql ~/skillprojects/social_network/db/postgresql/docker-entrypoint-initdb.d/