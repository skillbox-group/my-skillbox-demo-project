### Поднять сервисы
1. Все очистить (удалить image user_app, персистент директории): `~/Documents/SkillboxProject/my-skillbox-demo-project/scripts/all_clear.sh`
2. Все создать (image user_app, персистент директории): `~/Documents/SkillboxProject/my-skillbox-demo-project/scripts/all_prepare.sh`
3. Создать docker сеть: `docker network create social_network_net`
4. Запустить docker-compose: `~/Documents/SkillboxProject/my-skillbox-demo-project/scripts/docker_compose_run.sh`
5. Поднять сервис users в докере: `docker run --rm --network social_network_net -p 8008:8080 users_app`
6. Все остановить: `~/Documents/SkillboxProject/my-skillbox-demo-project/scripts/all_stop.sh`

