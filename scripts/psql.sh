#!/bin/bash

docker run --rm --network social_network_net --name social_network_db \
-e POSTGRES_DB=social_network_db \
-e POSTGRES_USER=owner \
-e POSTGRES_PASSWORD=owner_pass \
-v ~/skillprojects/social_network/db/postgresql/persistent/:/var/lib/postgresql/data \
-v ~/skillprojects/social_network/db/postgresql/docker-entrypoint-initdb.d/:/docker-entrypoint-initdb.d \
-p 5433:5432 \
postgres:15.2