#!/bin/bash

cd ~/Documents/SkillboxProject/my-skillbox-demo-project/
rm scripts/users/docker/users.jar

gradle bootjar

cp build/libs/users-0.0.1-SNAPSHOT.jar scripts/users/docker/users.jar
rm build/libs/users-0.0.1-SNAPSHOT.jar

cd scripts/users/docker/
docker build -t users_app .