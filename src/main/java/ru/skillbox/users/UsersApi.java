package ru.skillbox.users;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsersApi {

    public static void main(String[] args) {
        SpringApplication.run(UsersApi.class, args);
    }

}
