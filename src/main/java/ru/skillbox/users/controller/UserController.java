package ru.skillbox.users.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import ru.skillbox.users.entity.User;
import ru.skillbox.users.service.UserService;

import java.util.List;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    private static final Logger LOGGER = LogManager.getLogger();

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    User createUser(@RequestBody User user) {
        LOGGER.info(">> POST /users, body: {}", user);
        return userService.createUser(user);
    }

    @GetMapping(path = "/{id}")
    User getUser(@PathVariable Long id) {
        LOGGER.info(">> GET /users/{}", id);
        return userService.getUser(id);
    }

    @GetMapping(path = "/{lastName}/{firstName}")
    List<User> getUserByNames(@PathVariable String lastName, @PathVariable String firstName) {
        LOGGER.info(">> GET /users/{}/{}", lastName, firstName);
        return userService.getUserByNames(lastName, firstName);
    }

    @GetMapping(path = "/deleted/{id}")
    User getDeletedUser(@PathVariable Long id) {
        LOGGER.info(">> GET /users/deleted/{}", id);
        return userService.getDeletedUser(id);
    }

    @PutMapping(path = "/{id}")
    User updateUser(@RequestBody User user, @PathVariable Long id) {
        LOGGER.info(">> PUT /users/{}, body: {}", id, user);
        checkMatchId(user, id);
        return userService.updateUser(user);
    }

    @DeleteMapping(path = "/{id}")
    User deleteUser(@PathVariable Long id) {
        LOGGER.info(">> DELETE /users/{}", id);
        return userService.deleteUser(id);
    }

    @GetMapping(path = "/all")
    List<User> getAllUsers() {
        LOGGER.info("GET /users/all");
        return userService.getAllUsers();
    }

    @GetMapping
    List<User> getActivityUsers() {
        LOGGER.info("GET /users");
        return userService.getActivityUsers();
    }

    @GetMapping(path = "/deleted")
    List<User> getDeletedUsers() {
        LOGGER.info("GET /users/deleted");
        return userService.getDeletedUsers();
    }

    @PutMapping(path = "/subscribe/{followerId}/{bloggerId}")
    void subscribeUser(@PathVariable Long followerId, @PathVariable Long bloggerId) {
        LOGGER.info(">> PUT /users/subscribe/{}/{}", followerId, bloggerId);
        if (followerId.compareTo(bloggerId) == 0) {
            LOGGER.error("!Ошибка - попытка подписаться на себя, id: {}", followerId);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        userService.addSubscription(followerId, bloggerId);
    }

    @PutMapping(path = "/unsubscribe/{followerId}/{bloggerId}")
    void unsubscribeUser(@PathVariable Long followerId, @PathVariable Long bloggerId) {
        LOGGER.info(">> PUT /users/unsubscribe/{}/{}", followerId, bloggerId);
        if (followerId.compareTo(bloggerId) == 0) {
            LOGGER.error("!Ошибка - попытка отписаться от себя, id: {}", followerId);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        userService.deleteSubscription(followerId, bloggerId);
    }

    private void checkMatchId(User user, Long id) {
        if (user.getId().compareTo(id) != 0) {
            LOGGER.error("!Ошибка - не соответствие id пользователя, body: {}, url: {}", user.getId(), id);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
