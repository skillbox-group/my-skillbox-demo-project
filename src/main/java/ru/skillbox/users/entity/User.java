package ru.skillbox.users.entity;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users", schema = "social_network_schema")
public class User {
    @Id
    @SequenceGenerator(name = "user_id_seq", sequenceName = "social_network_schema.user_id_seq", initialValue = 1, allocationSize = 20)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_seq")
    private Long id;
    private boolean active;
    private String login;
    @Column(name = "firstname")
    private String firstName;
    @Column(name = "lastname")
    private String lastName;
    private String patronymic;
    private String email;
    @Column(name = "birthdate")
    private LocalDate birthDate;
    @ElementCollection
    @CollectionTable(name = "users_followers", schema = "social_network_schema", joinColumns = @JoinColumn(name = "users_id"))
    @Column(name = "follower")
    private Set<Long> followers;
    @ElementCollection
    @CollectionTable(name = "users_bloggers", schema = "social_network_schema", joinColumns = @JoinColumn(name = "users_id"))
    @Column(name = "blogger")
    private Set<Long> bloggers;

    public User() {
        active = true;
        followers = new HashSet<>();
        bloggers = new HashSet<>();
    }

    public User(String login, String firstName, String lastName, String patronymic, String email, LocalDate birthDate) {
        this.active = true;
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.email = email;
        this.birthDate = birthDate;
    }

    public Long getId() {
        return id;
    }

    public boolean getActivity() {
        return active;
    }

    public void setActivity(boolean active) {
        this.active = active;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Set<Long> getFollowers() {
        return followers;
    }

    public Set<Long> getBloggers() {
        return bloggers;
    }

    public void addBlogger(Long id) {
        this.bloggers.add(id);
    }

    public void deleteBlogger(Long id) {
        this.bloggers.remove(id);
    }

    public void addFollower(Long id) {
        this.followers.add(id);
    }

    public void deleteFollower(Long id) {
        this.followers.remove(id);
    }

    @Override
    public String toString() {
        return (id == null ? "(" : "(ID: " + id + ", ") +
                (active ? "" : "УДАЛЕН, ") +
                login + ", " + firstName + " " + lastName + ", " +
                (followers.isEmpty() ? "" : "подписчиков: " + followers.size() + ", ") +
                (bloggers.isEmpty() ? "" : "подписок: " + bloggers.size() + ", ") +
                email + ", " + birthDate + ")";
    }
}
