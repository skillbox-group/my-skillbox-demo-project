package ru.skillbox.users.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.skillbox.users.entity.User;

import java.util.List;

@Service
public class RepositoryService {

    private static final Logger LOGGER = LogManager.getLogger();

    private final UserRepository userRepository;

    public RepositoryService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUser(Long id) {
        try {
            return userRepository.findById(id).orElseThrow(RuntimeException::new);
        } catch (RuntimeException e) {
            LOGGER.error("!Ошибка - не найден пользователь ID: {}", id);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    public boolean checkActivityUserById(User user) {
        return user.getActivity();
    }

    public List<User> findUser(String firstName, String lastName) {
        List<User> usersByLastName = userRepository.findByLastName(lastName);
        LOGGER.info("..найдено {} пользователей по фамилии: {}", usersByLastName.size(), lastName);
        List<User> usersByLastNameAndFirstName  = usersByLastName.stream().filter(e -> firstName.equals(e.getFirstName())).toList();
        LOGGER.info("..найдено {} пользователей с именем {} {}", usersByLastNameAndFirstName.size(), firstName, lastName);
        return usersByLastNameAndFirstName;
    }

    public List<User> findUserByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    public List<User> findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User saveUser(User user) {
        try {
            return userRepository.save(user);
        } catch (RuntimeException e) {
            LOGGER.error("!Ошибка - не сохранен пользователь: {} ", user);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public User deleteById(Long id) {
        User receivedUser = getUser(id);
        receivedUser.setActivity(false);
        saveUser(receivedUser);
        return getUser(id);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

}
