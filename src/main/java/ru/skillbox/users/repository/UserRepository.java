package ru.skillbox.users.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.skillbox.users.entity.User;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findByLastName(String lastName);

    List<User> findByEmail(String email);

    List<User> findByLogin(String login);

}
