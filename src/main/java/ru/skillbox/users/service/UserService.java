package ru.skillbox.users.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.skillbox.users.entity.User;
import ru.skillbox.users.repository.RepositoryService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    private static final Logger LOGGER = LogManager.getLogger();

    private final RepositoryService repositoryService;

    public UserService(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }

    public User createUser(User user) {
        String login = user.getLogin();
        String email = user.getEmail();
        if (usersByLoginAndEmail(login, email).isEmpty()) {
            return repositoryService.saveUser(user);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public User getUser(Long id) {
        User receivedUser = repositoryService.getUser(id);
        if (repositoryService.checkActivityUserById(receivedUser)) {
            return receivedUser;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    public List<User> getUserByNames(String lastName, String firstName) {
        List<User> users = repositoryService.findUser(firstName, lastName);
        if (users.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } else {
            return users;
        }
    }

    public User getDeletedUser(Long id) {
        User receivedUser = repositoryService.getUser(id);
        if (!repositoryService.checkActivityUserById(receivedUser)) {
            return receivedUser;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    public User updateUser(User user) {
        User receivedUser = getUser(user.getId());

        Long id = user.getId();
        String login = user.getLogin();
        String email = user.getEmail();

        List<User> existUsers = usersByLoginAndEmail(login, email);
        if (excludeUserById(id, existUsers).isEmpty()) {
            String firstName = user.getFirstName();
            String lastName = user.getLastName();
            String patronymic = user.getPatronymic();
            LocalDate birthDate = user.getBirthDate();

            receivedUser.setLogin(login);
            receivedUser.setFirstName(firstName);
            receivedUser.setLastName(lastName);
            receivedUser.setPatronymic(patronymic);
            receivedUser.setEmail(email);
            receivedUser.setBirthDate(birthDate);
            return repositoryService.saveUser(receivedUser);
        } else {
            LOGGER.info("..не обновлен пользователь ID: {}", id);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

    }

    private List<User> excludeUserById(Long id, List<User> users) {
        return users.stream().filter(e -> e.getId().compareTo(id) != 0).toList();
    }

    private List<User> usersByLoginAndEmail(String login, String email) {
        List<User> activityUsersByLogin = filterActivityUsers(repositoryService.findUserByLogin(login));
        List<User> activityUsersByEmail = filterActivityUsers(repositoryService.findUserByEmail(email));
        if (activityUsersByLogin.isEmpty() && activityUsersByEmail.isEmpty()) {
            return new ArrayList<>();
        } else {
            List<User> existUsers = new ArrayList<>();
            if (!activityUsersByLogin.isEmpty()) {
                List<Long> idList = activityUsersByLogin.stream().map(User::getId).toList();
                LOGGER.info("..логин {} есть в ID: {}", login, idList);
                existUsers.addAll(activityUsersByLogin);
            }
            if (!activityUsersByEmail.isEmpty()) {
                List<Long> idList = activityUsersByEmail.stream().map(User::getId).toList();
                LOGGER.info("..почта {} есть в ID: {}", email, idList);
                existUsers.addAll(activityUsersByEmail);
            }
            return existUsers;
        }

    }

    private List<User> filterActivityUsers(List<User> users) {
        return users.stream().filter(User::getActivity).toList();
    }

    public User deleteUser(Long id) {
        getUser(id).getFollowers().forEach(e -> getUser(e).deleteBlogger(id));
        User deletedUser = repositoryService.deleteById(id);
        LOGGER.info("> Удаление - удален пользователь с ID: {}", deletedUser.getId());
        return deletedUser;
    }

    public List<User> getAllUsers() {
        try {
            List<User> allUsers = repositoryService.findAll();
            LOGGER.info("..получены все пользователи, всего: {}", allUsers.size());
            return allUsers;
        } catch (RuntimeException e) {
            LOGGER.error("!Ошибка - пользователи не получены");
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public List<User> getActivityUsers() {
        List<User> activityUsers = filterActivityUsers(getAllUsers());
        LOGGER.info("..получены все активные пользователи, всего: {}", activityUsers.size());
        return activityUsers;
    }

    public List<User> getDeletedUsers() {
        List<User> allUsers = getAllUsers();
        List<User> deletedUsers = allUsers.stream().filter(e -> !e.getActivity()).toList();
        LOGGER.info("..получены все удаленные пользователи, всего: {}", deletedUsers.size());
        return deletedUsers;
    }

    public void addSubscription(Long followerId, Long bloggerId) {
        User followerUser = getUser(followerId);
        User bloggerUser = getUser(bloggerId);
        addBlogger(followerUser, bloggerId);
        addFollower(bloggerUser, followerId);
    }

    private void addBlogger(User followerUser, Long bloggerId) {
        if (!followerUser.getBloggers().contains(bloggerId)) {
            followerUser.addBlogger(bloggerId);
            repositoryService.saveUser(followerUser);
        } else {
            LOGGER.info("..подписка не осуществлена, ID: {} уже подписан на ID: {}", followerUser.getId(), bloggerId);
        }
    }

    private void addFollower(User bloggerUser, Long followerId) {
        if (!bloggerUser.getFollowers().contains(followerId)) {
            bloggerUser.addFollower(followerId);
            repositoryService.saveUser(bloggerUser);
        } else {
            LOGGER.info("..подписка не осуществлена, ID: {} уже имеет подписчика ID: {}", bloggerUser.getId(), followerId);
        }
    }

    public void deleteSubscription(Long followerId, Long bloggerId) {
        User followerUser = getUser(followerId);
        User bloggerUser = getUser(bloggerId);
        deleteBlogger(followerUser, bloggerId);
        deleteFollower(bloggerUser, followerId);
        LOGGER.info("..подписка отменена, ID: {} не подписан на ID: {}", followerId, bloggerId);
    }

    private void deleteBlogger(User followerUser, Long bloggerId) {
        if (followerUser.getBloggers().contains(bloggerId)) {
            followerUser.deleteBlogger(bloggerId);
            repositoryService.saveUser(followerUser);
        } else {
            LOGGER.info("..для пользователя ID: {} отменена подписка на ID: {}", followerUser.getId(), bloggerId);
        }
    }

    private void deleteFollower(User bloggerUser, Long followerId) {
        if (bloggerUser.getFollowers().contains(followerId)) {
            bloggerUser.deleteFollower(followerId);
            repositoryService.saveUser(bloggerUser);
        } else {
            LOGGER.info("..у пользователя ID: {} из подписчиков удален ID: {}", bloggerUser.getId(), followerId);
        }
    }

}
